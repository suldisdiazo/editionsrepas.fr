---
Edition: Editions REPAS
DépotLégal: 2018
Auteurs: Association La NEF, avec la collaboration de Michel Lulek
Titre: Pour que l'argent relie les Hommes
SousTitre: 40 années de réflexion et d'expérimentation
Préface: Préface de Patrick Viveret
Collection: Collection Pratiques Utopiques
ISBN : 978-2-919272-13-6
Pages: 160 pages
Prix: 17
Etat: Disponible
Résumé: |
    Faire circuler l'argent en respectant les humains et en créant du lien social, tel est le pari que font en 1978 une douzaine de personnes. S'inspirant des intuitions de Rudolf Steiner, elles vont expérimenter, pas à pas, différentes manières d'agir avec l'argent par le don ou par le prêt. Au sein de l'Association La NEF, créée en 1979, elles vont collecter des fonds qui seront ensuite affectés au soutien et au développement de projets où l'humain est toujours mis au centre.

    Malgré les obstacles, elles arriveront à imposer un nouveau regard sur l'argent, non pour que celui-ci serve à enrichir davantage ou à creuser des inégalités, mais pour qu'il puisse « relier les hommes ». C'est cette aventure qui est racontée dans ce livre.
Tags:
- Alternatives
- Economie sociale et solidaire
SiteWeb: http://www.association-lanef.org/
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/pour-que-l-argent-relie-les-hommes-association-la-nef?_ga=2.249233159.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272136-pour-que-l-argent-relie-les-hommes-40-annees-de-reflexion-et-d-experimentation-association-la-nef/
Couverture:
Couleurs:
   PageTitres: '#f3965d'
   Fond: ' #934C9A'
   Titre: '#fffff'
Adresse: 1 Rue François Laubeuf, 78400 Chatou
Latitude: 48.890783
Longitude: 2.154102
---
## Les auteurs

Ce récit a été écrit à partir de la transcription d'entretiens réalisés en 2013 par Béatrice Barras avec Jean-Pierre Bideau, Philippe Leconte, Serge Alquier et Catherine Nouyrit et d'entretiens complémentaires réalisés en 2014 et 2015 par Michel Lulek avec Jean-Pierre Caron, Marie-Thérèse Ducourau, Patrick Sirdey, Corinne Nouyrit-Seignez, Jean-Luc Seignez, Jean-Claude Detilleux, Béatrice Barras, Philippe Clairfayt et, à nouveau, Jean-Pierre Bideau, Serge Alquier, Philippe Leconte.



## Extrait

> L’invention de la communauté des donateurs-emprunteurs est la première innovation introduite dans le secteur financier français par La NEF. Alliant prêt et don, elle lie deux fonctions de l’argent qui sont la plupart du temps dissociées. Jouant sur l’addition de petites sommes, elle permet néanmoins de répondre à des besoins conséquents avec de faibles contributions. Reliant les donateurs ou les prêteurs à un projet clairement identifié, elle tisse une relation qui n’est pas seulement financière, mais humaine. Faisant circuler de l’argent, elle mobilise en réalité des individus pour une cause altruiste. Expérimentant en grandeur réelle de nouvelles solidarités financières, elle rend visible et effectif le pouvoir dont chacun dispose avec son argent.
> « *L’organisme ainsi créé devait certes servir à financer les initiatives agricoles et scolaires (…), mais il devait surtout être une sorte de laboratoire, ouvert à toutes personnes de bonne volonté, voulant expérimenter des pratiques de solidarité et de réciprocité dans l’épargne, les garanties, les crédits, la responsabilité sociale.* »
> -- page 66

## Le commentaire de Patrick Viveret

« *La vitrine bancaire que représente la Nef n'est que la face émergée d'un projet plus vaste qui est portée par l'association La NEF, née en 1978. Celle-ci n'a pas disparu, une fois la société financière du même nom créée dix ans plus tard. Elle a accompagné cette structure bancaire dans sa constitution et son développement, et elle poursuit toujours son action que ce soit en lançant des outils de don (un fonds de dotation) ou de formation (une université citoyenne). Mais au-delà de ces outils, c'est la réflexion qui les précède qui donne tout son sens à ce projet.*
*Les différentes qualités de l'argent (argent de consommation, argent de prêt, argent de don), la place de l'économie dans la trilogie républicaine (incarnant la fraternité), le regard porté sur l'humain comme un être qui ne se réduit pas à la vision utilitariste des économistes, constituent le fondement de cette aventure.* » - Patrick Viveret, préfacier, philosophe et magistrat à la Cour des Comptes

La collection ***Pratiques utopiques*** rassemble des livres qui ont l’ambition de montrer qu’il y a toujours place, ici et maintenant, comme hier et ailleurs, pour des réalisations qui se donnent d’autres priorités que le profit, la course à la consommation ou le tout à l’économie et qui inscrivent leur sens dans le concret de pratiques libres et solidaires.
Face au morcellement du travail, à la désertification des cam¬pagnes, à la déshumanisation dans les cités ou à l’exclusion, des entreprises, des groupes, des associations ou des individus apportent des réponses originales et adaptées à ces questions de société qui paraissent parfois insolubles.
Concrètement il s’agit de bâtir cet « autre monde possible » qui ne peut objectivement se décliner qu’au pluriel.

Exemples de démocratie économique, d’initiative citoyenne ou d’innovation sociale, elles bousculent également quelques sacro-saints principes de notre société marchande, démontrant au quotidien que l’association est plus enrichissante que la compétition, que la coopération vaut mieux que la concurrence ou que l’autogestion permet de reprendre le pouvoir sur sa vie.

*Pratiques utopiques* espère, par ce biais, encourager ceux qui sont insatisfaits du monde dans lequel ils vivent, à faire le pas vers d’autres possibles. L’utopie est à portée de main.

